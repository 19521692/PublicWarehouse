import React from 'react'
// import {Link} from 'react-route-dom'

const Navbar = () => {
    return (
        <div className = "nav-bar"> 
            <h1>Goom</h1>
            <ul className="nav-item">
                <li>Home</li>
                <li>About</li>
                <li>Sign In/Sign Up</li>
            </ul>
        </div>
    )
}

export default Navbar
